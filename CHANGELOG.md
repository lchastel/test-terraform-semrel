## [1.0.3](https://gitlab.com/lchastel/test-terraform-semrel/compare/1.0.2...1.0.3) (2024-05-23)


### Bug Fixes

* fix/unsupported-regex-op ([bba62a2](https://gitlab.com/lchastel/test-terraform-semrel/commit/bba62a2bea6f65469b787d7777f325f183b6241d))

## [1.0.2](https://gitlab.com/lchastel/test-terraform-semrel/compare/1.0.1...1.0.2) (2024-05-23)


### Bug Fixes

* trigger CI on tag ([336ca37](https://gitlab.com/lchastel/test-terraform-semrel/commit/336ca37644d89b5afdb9c5f08cc25e8ad0b22f79))

# 1.0.0 (2024-05-22)


### Features

* project creation ([9102431](https://gitlab.com/lchastel/test-terraform-semrel/commit/91024313d192c10a9d9bd986a8cc47b28c65e023))
